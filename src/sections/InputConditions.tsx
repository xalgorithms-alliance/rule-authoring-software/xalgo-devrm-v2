import React, { FC, useContext } from 'react';
import { Icon, IconButton } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const InputConditions: FC = () => {
  const { t } = useXaTranslation();
  const { input_conditions, actions } = useContext(RuleContext);

  return (
    <>
      <h4 style={{ marginBottom: 20, marginTop: 30 }}>{t('inputStatements.title')}</h4>
      <div style={{ marginBottom: 20 }}>
        <IconButton icon={<Icon icon="plus" size="lg" />} onClick={() => actions.newInputCondition()}>
          New Input Condition
        </IconButton>
        <IconButton
          icon={<Icon icon="plus" size="lg" />}
          onClick={() => actions.newInputOutputCase()}
          disabled={!(input_conditions && input_conditions.length > 0)}
          style={{ marginLeft: 10 }}
        >
          New Input Condition Case
        </IconButton>
      </div>
      <div>
        {input_conditions &&
          input_conditions.map((value, index) => {
            const sentence = value.sentence;
            return (
              <div key={index}>
                <p>{`"${sentence?.determiner} ${sentence?.past_participle_verb} ${sentence?.subject_noun} ${sentence?.object_nounorverb} ${sentence?.auxiliary_verb} ${sentence?.object_descriptor}"`}</p>
                <div>
                  {value.logic_values &&
                    value.logic_values.map((value, index) => {
                      return <span key={index}>{`${value.scenario} = '${value.value}' `}</span>;
                    })}
                </div>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default InputConditions;
