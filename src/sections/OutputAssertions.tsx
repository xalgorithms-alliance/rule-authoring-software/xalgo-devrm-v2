import React, { FC, useContext } from 'react';
import { Icon, IconButton } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const OutputAssertions: FC = () => {
  const { t } = useXaTranslation();
  const { output_assertions, actions } = useContext(RuleContext);

  return (
    <>
      <h4 style={{ marginBottom: 20, marginTop: 30 }}>{t('outputAssertions.title')}</h4>
      <div style={{ marginBottom: 20 }}>
        <IconButton icon={<Icon icon="plus" size="lg" />} onClick={() => actions.newOutputAssertion()}>
          New Output Assertion
        </IconButton>
        <IconButton
          icon={<Icon icon="plus" size="lg" />}
          onClick={() => actions.newInputOutputCase()}
          disabled={!(output_assertions && output_assertions.length > 0)}
          style={{ marginLeft: 10 }}
        >
          New Output Assertion Case
        </IconButton>
      </div>
      <div>
        {output_assertions &&
          output_assertions.map((value, index) => {
            const sentence = value.sentence;
            return (
              <div key={index}>
                <p>{`"${sentence?.determiner} ${sentence?.past_participle_verb} ${sentence?.subject_noun} ${sentence?.object_nounorverb} ${sentence?.auxiliary_verb} ${sentence?.object_descriptor}"`}</p>
                <div>
                  {value.logic_values &&
                    value.logic_values.map((value, index) => {
                      return <span key={index}>{`${value.scenario} = '${value.value}' `}</span>;
                    })}
                </div>
              </div>
            );
          })}
      </div>
    </>
  );
};

export default OutputAssertions;
