/* eslint @typescript-eslint/no-non-null-assertion: 0 */

import produce from 'immer';
import React, { FC, useContext, useEffect, useMemo, useState } from 'react';
import {
  Button, ButtonToolbar, ControlLabel, Form,
  FormControl,
  FormGroup, HelpBlock, Icon,
  IconButton,
  Modal, Tooltip, Whisper
} from 'rsuite';
import { RuleContext, SetSentence } from '../data/RuleProvider';
import Case0 from '../data/svg/0.svg';
import Case1 from '../data/svg/1.svg';
import Case10 from '../data/svg/10.svg';
import Case11 from '../data/svg/11.svg';
import { useXaTranslation } from '../i18n';
import { Rule } from '../rule';
import { ArrayElement, InputCondition, OutputAssertion } from '../rule-elements';

// TODO: These should be moved to a constants file.
const ic_cases: string[] = ['00', '01', '10', '11'];
const ic_cases_len = ic_cases.length;

const oa_cases: string[] = ['00', '01', '10', '11'];
const oa_cases_len = oa_cases.length;

/**
 * Responsible for showing a modal to edit the contents of a rule sentence.
 */
const SentenceEditor: FC<{
  sentence: InputCondition | OutputAssertion | undefined;
  index: number;
  setRows: SetSentence;
}> = ({ sentence, index, setRows }) => {
  // Each sentence has an independent editing modal.
  const [open, setOpen] = useState<boolean>(false);
  const [deletionModalOpen, setDeletionModalOpen] = useState<boolean>(false);

  // When opened, the current sentence state is loaded into a smaller state
  // object to save on resources when changing strings within the object.
  const [tempSentence, setTempSentence] = useState<InputCondition['sentence']>();
  useEffect(() => {
    setTempSentence(sentence);
  }, [open, sentence, setTempSentence]);

  const sentenceString = useMemo(() => {
    return `${tempSentence?.determiner} ${tempSentence?.subject_noun} ${tempSentence?.past_participle_verb} ${tempSentence?.auxiliary_verb} ${tempSentence?.object_descriptor} ${tempSentence?.object_nounorverb}`;
  }, [tempSentence]);

  if (
    !sentence ||
    sentence === undefined ||
    !tempSentence ||
    tempSentence === undefined ||
    index === undefined ||
    !setRows
  )
    return null;

  return (
    <>
      {/** might look good with justifyContent: 'space-between' */}
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
        <p style={{ paddingRight: 5 }}>
          <span className="faded-quote">"</span>
          {sentenceString}
          <span className="faded-quote">"</span>
        </p>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Button appearance="subtle" onClick={() => setOpen(true)}>
            <Icon icon="pencil" />
          </Button>
          <Button appearance="subtle" color="red" onClick={() => setDeletionModalOpen(true)}>
            <Icon icon="trash" />
          </Button>
        </div>
      </div>

      {/** Sentence Deletion Modal */}
      <Modal show={deletionModalOpen} onHide={() => setDeletionModalOpen(false)} size="sm">
        <Modal.Header>
          <Modal.Title>Confirm Deletion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            The sentence will be <b>permanently deleted</b>:
          </p>
          <p style={{ paddingLeft: '1rem' }}>
            <span className="faded-quote">"</span>
            {sentenceString}
            <span className="faded-quote">"</span>
          </p>
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <IconButton
              appearance="primary"
              type="submit"
              icon={<Icon style={{ textAlign: 'center' }} icon="check" />}
              color="red"
              onClick={() => {
                setRows((prev) =>
                  produce(prev, (draft) => {
                    if (!draft) return;
                    draft = draft.splice(index, 1);
                  })
                );
                setDeletionModalOpen(false);
              }}
            >
              Delete Sentence
            </IconButton>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>

      {/** Sentence Editing Modal */}
      <Modal show={open} onHide={() => setOpen(false)} size="lg">
        <Modal.Header>
          <Modal.Title>Edit Sentence</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/** Heading presents tempSentence in full. */}
          <h4>
            <span className="faded-quote">"</span>
            {sentenceString}
            <span className="faded-quote">"</span>
          </h4>
          <br />
          {/** Table visualises tempSentence in its components. */}
          <table id={'sentence-table'}>
            <tbody>
              <tr>
                <td>
                  <h5><span className="faded-quote">"</span>{`${tempSentence.determiner}`}</h5>
                </td>
                <td>
                  <h5>{`${tempSentence.subject_noun}`}</h5>
                </td>
                <td>
                  <h5>{`${tempSentence.past_participle_verb}`}</h5>
                </td>
                <td>
                  <h5>{`${tempSentence.auxiliary_verb}`}</h5>
                </td>
                <td>
                  <h5>{`${tempSentence.object_descriptor}`}</h5>
                </td>
                <td>
                  <h5>{`${tempSentence.object_nounorverb}`}<span className="faded-quote">"</span></h5>
                </td>
              </tr>
              <tr>
                <td>Determiner</td>
                <td>Subject Noun</td>
                <td>Past Participle Verb</td>
                <td>Auxilliary Verb</td>
                <td>Object Descriptor</td>
                <td>Object Noun/Verb</td>
              </tr>
            </tbody>
          </table>
          <br />
          {/** Editing Components. */}
          <Form
            onSubmit={(checkStatus, event) => {
              console.log(`Checkstatus: ${checkStatus}`);
              console.log(event);
              console.log(event.target);
              setRows((prev) =>
                produce(prev, (draft) => {
                  draft![index].sentence = tempSentence;
                })
              );
              setOpen(false);
            }}
            formValue={tempSentence}
            onChange={(formValue) => {
              const value = (formValue as Partial<InputCondition['sentence']>) || {};
              setTempSentence((prev) =>
                produce(prev, (draft) => {
                  Object.keys(value).forEach((key) => {
                    draft![key] = value[key];
                  });
                })
              );
            }}
          >
            <FormGroup>
              <ControlLabel>Determiner</ControlLabel>
              <FormControl name="determiner"></FormControl>
              <HelpBlock tooltip>(the-this-these-a) the | this | these | a</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Subject Noun</ControlLabel>
              <FormControl name="subject_noun"></FormControl>
              <HelpBlock tooltip>(word-phrase) word or phrase</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Past Participle Verb</ControlLabel>
              <FormControl name="past_participle_verb"></FormControl>
              <HelpBlock tooltip>(word-phrase) word or phrase referring to the subject noun</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Auxiliary Verb</ControlLabel>
              <FormControl name="auxiliary_verb"></FormControl>
              <HelpBlock tooltip>(be-do-have) be | do | have ; optionally with MUST, MAY, SHOULD, or NOT</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Object Descriptor</ControlLabel>
              <FormControl name="object_descriptor"></FormControl>
              <HelpBlock tooltip>(adjective-verb-arithmetic) Describes the object.</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ControlLabel>Object</ControlLabel>
              <FormControl name="object_nounorverb"></FormControl>
              <HelpBlock tooltip>(Noun Or Verb) Optionally with a preposition, the focus of the sentence.</HelpBlock>
            </FormGroup>
            <FormGroup>
              <ButtonToolbar>
                <IconButton appearance="primary" type="submit" icon={<Icon icon="check" />}>
                  Validate and Apply Changes
                </IconButton>
                <IconButton
                  appearance="primary"
                  type="submit"
                  icon={<Icon icon="trash" />}
                  color="red"
                  onClick={() => setDeletionModalOpen(true)}
                >
                  Delete Sentence
                </IconButton>
              </ButtonToolbar>
            </FormGroup>
          </Form>
          <br />
        </Modal.Body>
      </Modal>
    </>
  );
};

/**
 * Renders a single Input-Conditions or Output-Assertions table.
 */
const TableSection: FC<{
  sentences: InputCondition[] | OutputAssertion[];
  setRows: React.Dispatch<React.SetStateAction<Rule['input_conditions'] | Rule['output_assertions']>>;
  isInputCondition?: boolean;
  showReorderControls?: boolean;
}> = ({ sentences, setRows, isInputCondition = false, showReorderControls = false }) => {
  const { actions } = useContext(RuleContext);

  return (
    <>
      {sentences?.map((value, index) => {
        const sentence = value.sentence;
        const sentenceIndex = index;
        return (
          <tr key={index}>
            <td>
              <SentenceEditor sentence={sentence} index={sentenceIndex} setRows={setRows} />
            </td>
            {value.logic_values?.map((value, index) => {
              const caseIndex: number = index;
              const caseValue: string = value.value || '00';

              // Set SVG icon for the button.
              let icon = Case0;
              switch (caseValue) {
                case '01':
                  icon = Case1;
                  break;
                case '10':
                  icon = Case10;
                  break;
                case '11':
                  icon = Case11;
                  break;
              }
              return (
                <td
                  key={index}
                  onClick={() => {
                    console.log(`Attempting to edit ${sentenceIndex}-${caseIndex}`);
                  }}
                >
                  <Button
                    style={{ padding: 0, margin: 0, height: '50px', width: '50px' }}
                    appearance="subtle"
                    onClick={() => {
                      const cases = isInputCondition ? ic_cases : oa_cases;
                      const cases_len = isInputCondition ? ic_cases_len : oa_cases_len;
                      const nextCaseValue = cases[(cases.indexOf(caseValue) + 1) % cases_len];
                      console.log('Setting case to ' + nextCaseValue);
                      setRows((prev) =>
                        produce(prev, (draft) => {
                          try {
                            draft![sentenceIndex].logic_values![caseIndex].value = nextCaseValue;
                          } catch (error) {
                            console.error(error);
                          }
                        })
                      );
                    }}
                  >
                    <Icon icon={icon} size="3x" />
                  </Button>
                </td>
              );
            })}
            {showReorderControls && (
              <>
                <td>
                  <Button
                    appearance="subtle"
                    onClick={() => actions.moveSentence(index, index - 1, setRows)}
                    disabled={index === 0}
                  >
                    <Icon icon="up"></Icon>
                  </Button>
                </td>
                <td>
                  <Button
                    appearance="subtle"
                    onClick={() => actions.moveSentence(index, index + 1, setRows)}
                    disabled={index === sentences.length - 1}
                  >
                    <Icon icon="down"></Icon>
                  </Button>
                </td>
              </>
            )}
          </tr>
        );
      })}
    </>
  );
};

/**
 *
 * @returns
 */
const RenameCaseButton: FC<{
  value:
  | ArrayElement<NonNullable<InputCondition['logic_values']>>
  | ArrayElement<NonNullable<OutputAssertion['logic_values']>>
  | undefined;
  index: number;
}> = ({ value, index }) => {
  const { actions } = useContext(RuleContext);
  const [open, setOpen] = useState<boolean>(false);
  const [name, setName] = useState<string>('');

  if (!value || index === undefined) return null;
  return (
    <>
      <Button appearance="subtle" onClick={() => setOpen(true)}>
        <Icon icon="pencil" />
      </Button>
      <>
        <Modal show={open} onHide={() => setOpen(false)} size="sm">
          <Modal.Header>
            <Modal.Title>{`Rename Case "${value.scenario}"`}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <FormGroup>
                <ControlLabel>New Name</ControlLabel>
                <FormControl value={name} onChange={(value) => setName(value)} />
                <HelpBlock tooltip>Try to choose a name that is less than five characters.</HelpBlock>
              </FormGroup>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <ButtonToolbar>
              <IconButton
                appearance="primary"
                icon={<Icon style={{ textAlign: 'center' }} icon="check" />}
                onClick={() => {
                  actions.renameCase(name, index);
                  setOpen(false);
                }}
              >
                Rename Case
              </IconButton>
            </ButtonToolbar>
          </Modal.Footer>
        </Modal>
      </>
    </>
  );
};

/**
 * Renders everything needed to display and edit rule sentences.
 */
const SentenceTable: FC = () => {
  const { t } = useXaTranslation();
  const { input_conditions, setInputConditions, output_assertions, setOutputAssertions, actions } =
    useContext(RuleContext);

  const [showReorderControls, setShowReorderControls] = useState<boolean>(false);

  const headerRowData: InputCondition['logic_values'] | OutputAssertion['logic_values'] =
    (input_conditions && input_conditions.length > 0 && input_conditions[0] && input_conditions[0].logic_values) ||
    (output_assertions && output_assertions.length > 0 && output_assertions[0] && output_assertions[0].logic_values) ||
    [];

  return (
    <>
      <h4 style={{ marginBottom: 20, marginTop: 30 }}>{t('sentenceTable.title')}</h4>
      <div style={{ marginBottom: 20 }}>
        <ButtonToolbar>
          <IconButton icon={<Icon icon="plus" size="lg" />} onClick={() => actions.newInputCondition()}>
            {t('sentenceTable.buttons.newInputCondition')}
          </IconButton>
          <IconButton icon={<Icon icon="plus" size="lg" />} onClick={() => actions.newOutputAssertion()}>
            {t('sentenceTable.buttons.newOutputAssertion')}
          </IconButton>
          <IconButton
            icon={<Icon icon="plus" size="lg" />}
            onClick={() => actions.newInputOutputCase()}
            disabled={
              !(input_conditions && input_conditions.length > 0) && !(output_assertions && output_assertions.length > 0)
            }
          >
            {t('sentenceTable.buttons.newRuleCase')}
          </IconButton>
          <IconButton
            icon={<Icon icon={showReorderControls ? 'eye-slash' : 'eye'} />}
            onClick={() => setShowReorderControls((prev) => !prev)}
          >
            {showReorderControls ? 'Hide Organization Controls' : 'Show Organization Controls'}
          </IconButton>
        </ButtonToolbar>
      </div>
      <div style={{ overflowX: 'scroll', paddingBottom: '1rem' }}>
        <table id="input-output-table">
          {/** Header Row */}
          <thead>
            <tr>
              <th style={{ minWidth: '300px' }}>{t('sentenceTable.sections.inputConditions')}</th>
              {headerRowData.map((value, index) => (
                <th key={index}>{value.scenario}</th>
              ))}
              <th>
                {((input_conditions && input_conditions.length > 0) ||
                  (output_assertions && output_assertions.length > 0)) && (
                    <Whisper
                      placement="left"
                      trigger="hover"
                      speaker={<Tooltip>{t('sentenceTable.buttons.newRuleCase')}</Tooltip>}
                    >
                      <IconButton
                        icon={<Icon icon="plus" size="lg" style={{ textAlign: 'center' }} />}
                        onClick={() => actions.newInputOutputCase()}
                        appearance="subtle"
                        disabled={
                          !(input_conditions && input_conditions.length > 0) &&
                          !(output_assertions && output_assertions.length > 0)
                        }
                      ></IconButton>
                    </Whisper>
                  )}
              </th>
            </tr>
          </thead>
          <tbody>
            <TableSection
              sentences={input_conditions || []}
              setRows={setInputConditions}
              isInputCondition={true}
              showReorderControls={showReorderControls}
            />
            <tr>
              <td>
                {((input_conditions && input_conditions.length > 0) ||
                  (output_assertions && output_assertions.length > 0)) && (
                    <Whisper
                      placement="right"
                      trigger="hover"
                      speaker={<Tooltip>{t('sentenceTable.buttons.newInputCondition')}</Tooltip>}
                    >
                      <IconButton
                        appearance="subtle"
                        icon={<Icon icon="plus" style={{ textAlign: 'center' }} />}
                        onClick={() => actions.newInputCondition()}
                      ></IconButton>
                    </Whisper>
                  )}
              </td>
            </tr>
          </tbody>
          <thead>
            <tr>
              <th>{t('sentenceTable.sections.outputAssertions')}</th>
              {headerRowData.map((value, index) => (
                <th key={index}>{value.scenario}</th>
              ))}
              <th>
                {((input_conditions && input_conditions.length > 0) ||
                  (output_assertions && output_assertions.length > 0)) && (
                    <Whisper
                      placement="left"
                      trigger="hover"
                      speaker={<Tooltip>{t('sentenceTable.buttons.newRuleCase')}</Tooltip>}
                    >
                      <IconButton
                        icon={<Icon icon="plus" size="lg" style={{ textAlign: 'center' }} />}
                        onClick={() => actions.newInputOutputCase()}
                        appearance="subtle"
                        disabled={
                          !(input_conditions && input_conditions.length > 0) &&
                          !(output_assertions && output_assertions.length > 0)
                        }
                      ></IconButton>
                    </Whisper>
                  )}
              </th>
            </tr>
          </thead>
          <tbody>
            <TableSection
              sentences={output_assertions || []}
              setRows={setOutputAssertions}
              isInputCondition={false}
              showReorderControls={showReorderControls}
            />
            <tr>
              <td>
                {((input_conditions && input_conditions.length > 0) ||
                  (output_assertions && output_assertions.length > 0)) && (
                    <Whisper
                      placement="right"
                      trigger="hover"
                      speaker={<Tooltip>{t('sentenceTable.buttons.newOutputAssertion')}</Tooltip>}
                    >
                      <IconButton
                        appearance="subtle"
                        icon={<Icon icon="plus" style={{ textAlign: 'center' }} />}
                        onClick={() => actions.newOutputAssertion()}
                        disabled={
                          !(input_conditions && input_conditions.length > 0) &&
                          !(output_assertions && output_assertions.length > 0)
                        }
                      ></IconButton>
                    </Whisper>
                  )}
              </td>
            </tr>
          </tbody>
          {showReorderControls && (
            <>
              <thead>
                <tr>
                  <th>{t('sentenceTable.sections.manageCases')}</th>
                  {headerRowData.map((value, index) => (
                    <th key={index}>{value.scenario}</th>
                  ))}
                </tr>
              </thead>
              {headerRowData.length > 0 && (
                <tbody>
                  <tr>
                    <td>{t('general.delete')}</td>
                    {headerRowData.map((value, index) =>
                      value !== undefined ? (
                        <th key={index}>
                          <Button appearance="subtle" color="red" onClick={() => actions.deleteCase(index)}>
                            <Icon icon="trash" />
                          </Button>
                        </th>
                      ) : null
                    )}
                  </tr>
                  <tr>
                    <td>{t('general.rename')}</td>
                    {headerRowData.map((value, index) => (
                      <th key={index}>
                        <RenameCaseButton value={value} index={index} />
                      </th>
                    ))}
                  </tr>
                  <tr>
                    <td>{t('general.moveRight')}</td>
                    {headerRowData.map((value, index) => (
                      <th key={index}>
                        <Button
                          appearance="subtle"
                          onClick={() => actions.moveCase(index, index + 1)}
                          disabled={index === headerRowData.length - 1}
                        >
                          <Icon icon="right" />
                        </Button>
                      </th>
                    ))}
                  </tr>
                  <tr>
                    <td>{t('general.moveLeft')}</td>
                    {headerRowData.map((value, index) => (
                      <th key={index}>
                        <Button
                          appearance="subtle"
                          onClick={() => actions.moveCase(index, index - 1)}
                          disabled={index === 0}
                        >
                          <Icon icon="left" />
                        </Button>
                      </th>
                    ))}
                  </tr>
                </tbody>
              )}
            </>
          )}
        </table>
      </div>
      <hr />
    </>
  );
};

export default SentenceTable;
