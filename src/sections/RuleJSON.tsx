import React, { FC, useContext } from 'react';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const RuleJSON: FC = () => {
  const { t } = useXaTranslation();
  const { actions } = useContext(RuleContext);

  return (
    <>
      <h4 style={{ marginTop: 30 }}>{t('ruleJSON.title')}</h4>
      <div style={{ marginBottom: 30 }}>
        <pre>{JSON.stringify(actions.getRule(), null, 2)}</pre>
      </div>
    </>
  );
};

export default RuleJSON;
