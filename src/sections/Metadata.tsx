import React, { FC, useContext } from 'react';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const Metadata: FC = () => {
  const { t } = useXaTranslation();
  const { metadata } = useContext(RuleContext);

  return (
    <>
      <h4>{t('metadata.title')}</h4>
      <pre>{JSON.stringify(metadata, null, 4)}</pre>
    </>
  );
};

export default Metadata;
