import produce from 'immer';
import React, { FC, useContext, useState } from 'react';
import { Button, ControlLabel, Form, FormControl, FormGroup, HelpBlock, Schema } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const AboutRuleForm: FC = () => {
  //Localization hook
  const { t } = useXaTranslation();

  // Rule data
  const { metadata, setMetadata } = useContext(RuleContext);

  //stores form data
  const [formError, setFormError] = useState<Record<string, any>>({});

  //reference required for the rsuite form component
  let form: any = undefined;

  //Rule form data model
  const ruleData = Schema.Model({
    title: Schema.Types.StringType()
      .maxLength(2000, t('aboutRuleForm.formMsg.ruleNameLimit'))
      .isRequired(t('aboutRuleForm.formMsg.isRequired')),
    description: Schema.Types.StringType()
      .maxLength(20000, t('aboutRuleForm.formMsg.ruleDescriptionLimit'))
      .isRequired(t('aboutRuleForm.formMsg.isRequired')),
    version: Schema.Types.StringType()
      .maxLength(20, t('aboutRuleForm.formMsg.ruleVersionLimit'))
      .isRequired(t('aboutRuleForm.formMsg.isRequired')),
    criticality: Schema.Types.StringType().isRequired(t('aboutRuleForm.formMsg.isRequired')),
    url: Schema.Types.StringType()
      .isURL(t('aboutRuleForm.formMsg.ruleURLLimit'))
      .isRequired(t('aboutRuleForm.formMsg.isRequired')),
  });

  return (
    <Form
      onChange={(newData) =>
        setMetadata((prev) =>
          produce(prev, (draft) => {
            if (!prev || !draft || !draft.rule) return;
            draft.rule = newData;
          })
        )
      }
      onCheck={(newError) => setFormError(newError)}
      formValue={metadata?.rule}
      formError={formError}
      model={ruleData}
      ref={(ref: any) => (form = ref)}
    >
      <h5>{t('aboutRuleForm.firstSectionTitle')}</h5>
      <FormGroup>
        <ControlLabel>{t('aboutRuleForm.ruleName')}</ControlLabel>
        <FormControl rows={3} name="title" componentClass="textarea" />
        <HelpBlock tooltip>{t('aboutRuleForm.formMsg.toolTip')}</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>{t('aboutRuleForm.ruleDescription')}</ControlLabel>
        <FormControl rows={5} name="description" componentClass="textarea" />
        <HelpBlock tooltip>{t('aboutRuleForm.formMsg.toolTip')}</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>{t('aboutRuleForm.ruleVersion')}</ControlLabel>
        <FormControl name="version" />
        <HelpBlock tooltip>{t('aboutRuleForm.formMsg.toolTip')}</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>{t('aboutRuleForm.ruleCriticality')}</ControlLabel>
        <FormControl name="criticality" />
        <HelpBlock tooltip>{t('aboutRuleForm.formMsg.toolTip')}</HelpBlock>
      </FormGroup>
      <FormGroup>
        <ControlLabel>{t('aboutRuleForm.ruleUrl')}</ControlLabel>
        <FormControl name="url" />
        <HelpBlock tooltip>{t('aboutRuleForm.formMsg.toolTip')}</HelpBlock>
      </FormGroup>
      {/* <h5>{t('aboutRuleForm.thirdSectionTitle')}</h5>
      <FormGroup>
        <Button appearance="ghost">{t('aboutRuleForm.addBtn')}</Button>
      </FormGroup> */}
    </Form>
  );
};

export default AboutRuleForm;
