import React, { CSSProperties, FC } from 'react';
import { Dropdown, Icon } from 'rsuite';
import { useXaTranslation, resources } from '../i18n';

// Adapted from https://github.com/Small-Minds/Democracy-PWA-V3/blob/master/src/components/LanguagePicker.tsx

const LanguageSwitcher: FC<{ style?: CSSProperties }> = ({ style }) => {
  //Set Up translation hook
  const { i18n } = useXaTranslation();

  // Get the language from the browser.
  const langArr = i18n.language.split('-');
  const lang = langArr.length === 0 ? '?' : langArr[0].toUpperCase();

  return (
    <Dropdown
      style={style}
      icon={<Icon icon="globe" />}
      title={lang}
      onSelect={(key) => i18n.changeLanguage(key)}
      placement="bottomStart"
    >
      {Object.entries(resources).map((lang, key) => (
        <Dropdown.Item key={key} eventKey={lang}>
          <b>{lang[0].toUpperCase()}</b> &middot; {lang[1].name}
        </Dropdown.Item>
      ))}
    </Dropdown>
  );
};

export default LanguageSwitcher;
