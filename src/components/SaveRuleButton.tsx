import React, { CSSProperties, FC, useContext, useState } from 'react';
import { Icon, IconButton, Notification } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';

const SaveRuleButton: FC<{ onClick?: () => void; style?: CSSProperties }> = ({ onClick, style }) => {
  const { t } = useXaTranslation();
  const { appState, actions } = useContext(RuleContext);
  const [saved, setSaved] = useState(false);

  const errorNotification = (error: string) => {
    console.error(error);
    Notification['error']({
      title: t('errors.couldNotSave.title'),
      description: t('errors.couldNotSave.description') + ` ${error}`,
      duration: 5000,
    });
  };

  const indicateSaveAction = () => {
    setSaved(true);
    setTimeout(() => {
      setSaved(false);
    }, 2000);
  };

  return (
    <>
      <IconButton
        icon={saved ? <Icon icon="check" /> : <Icon icon="save" />}
        style={style}
        disabled={!appState.filePath}
        appearance={saved ? 'primary' : 'default'}
        color={saved ? 'green' : undefined}
        onClick={() => {
          try {
            actions.save();
            if (onClick) onClick();
            indicateSaveAction();
          } catch (error) {
            errorNotification(error);
          }
        }}
      >
        {t('general.save')}
      </IconButton>
    </>
  );
};

export default SaveRuleButton;
