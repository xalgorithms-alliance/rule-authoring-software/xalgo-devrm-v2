import produce from 'immer';
import path from 'path';
import React, { CSSProperties, FC, useContext, useState } from 'react';
import { Button, Icon, IconButton, List, Modal } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';
import FolderPicker from './FolderPicker';

const RulePicker: FC<{ onClick?: () => void; style?: CSSProperties }> = ({ onClick, style }) => {
  const { t } = useXaTranslation();
  const [open, setOpen] = useState(false);
  const { appState, actions } = useContext(RuleContext);

  const files = appState.files.filter((x) => x.endsWith('.rule.json'));

  return (
    <>
      <Modal show={open} onHide={() => setOpen(false)}>
        <Modal.Header>
          <Modal.Title>{t('rulePicker.title')}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p style={{ marginTop: 4, marginBottom: 10 }}>
            <b>{'Folder: '}</b>
            <code>{appState.folderPath}</code>
          </p>
          <FolderPicker />
          {appState.folderPath && (
            <>
              <List style={{ marginTop: 10 }}>
                {files.length === 0 && (
                  <List.Item>
                    <p>{t('rulePicker.noRules')}</p>
                  </List.Item>
                )}
                {files.map((value: string, index: number) => (
                  <List.Item key={index}>
                    <Button
                      onClick={() => {
                        actions.open(path.join(appState.folderPath, value));
                        setOpen(false);
                      }}
                    >
                      {value}
                    </Button>
                  </List.Item>
                ))}
              </List>
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            style={style}
            onClick={() => {
              setOpen(false);
              if (onClick) onClick();
            }}
          >
            {t('general.close')}
          </Button>
        </Modal.Footer>
      </Modal>
      <IconButton
        style={style}
        icon={<Icon icon="folder-open" />}
        onClick={() => {
          setOpen(true);
          if (onClick) onClick();
        }}
      >
        {t('rulePicker.open')}
      </IconButton>
    </>
  );
};

export default RulePicker;
