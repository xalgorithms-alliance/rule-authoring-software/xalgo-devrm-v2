import electron from 'electron';
import fs from 'fs';
import produce from 'immer';
import React, { FC, useContext } from 'react';
import { Button, Icon, IconButton } from 'rsuite';
import { RuleContext } from '../data/RuleProvider';
import { useXaTranslation } from '../i18n';
// import path from 'path';

const FolderPicker: FC = () => {
  const { appState, setAppState, actions} = useContext(RuleContext);
  const { t } = useXaTranslation();


  return (
    <>
      <Button
        appearance="primary"
        onClick={() => {
          electron.remote.dialog
            .showOpenDialog({
              properties: ['openDirectory'],
            })
            .then((value: electron.OpenDialogReturnValue) => {
              console.log(value);
              const path: string | undefined = value.filePaths.pop();
              if (path) {
                actions.openFolder(path);
              } else {
                console.error('No path specified.');
              }
            });
        }}
      >
        {appState.folderPath ? t('folderPicker.changeFolder') : t('folderPicker.setFolder')}
      </Button>
      <IconButton
        style={{ marginLeft: 10 }}
        icon={<Icon icon="refresh" />}
        disabled={!appState.folderPath}
        onClick={() => actions.openFolder(appState.folderPath)}
      >
        Refresh
      </IconButton>
    </>
  );
};

export default FolderPicker;
