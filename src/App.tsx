import * as React from 'react';
import { hot } from 'react-hot-loader';
import { I18nextProvider } from 'react-i18next';
import RuleProvider from './data/RuleProvider';
import i18n from './i18n';
import Landing from './pages/Landing';

const App = () => (
  <I18nextProvider i18n={i18n}>
    <RuleProvider>
      <Landing />
    </RuleProvider>
  </I18nextProvider>
);

export default hot(module)(App);
