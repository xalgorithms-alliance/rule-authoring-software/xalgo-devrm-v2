import i18n, { TFunction, TFunctionKeys, TOptions } from 'i18next';
import { initReactI18next, useTranslation } from 'react-i18next';

import EN from './locales/en.json';

export const resources = {
  en: {
    name: 'English',
    translation: EN,
  },
};

i18n.use(initReactI18next).init({
  resources: resources,
  lng: 'en',
  fallbackLng: 'en',
  react: {
    bindI18n: 'languageChanged loaded',
    bindI18nStore: 'added removed',
    nsMode: 'default',
  },
  detection: {
    // order and from where user language should be detected
    order: ['querystring', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],
  },
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;

/**
 * Custom translation hook wrapper.
 */

type xaTranslation = {
  t: TFunction;
  i18n: typeof i18n;
  ready: boolean;
};

/**
 * Alternative translation function.
 */
const t = (f: TFunction, key: TFunctionKeys, options: TOptions) => {
  return f(key, options);
};

/**
 * Translation wrapper function enabling alternative lookups.
 * @returns t translation funciton
 */
export function useXaTranslation(): xaTranslation {
  const translation = useTranslation();
  return {
    t: ((key: TFunctionKeys, options: TOptions) => t(translation.t, key, options)) as TFunction,
    i18n: translation.i18n,
    ready: translation.ready,
  };
}
